import React from 'react';
import {Container, Col, Row} from "react-bootstrap";
import './Navbar.css';

function LeftCol() {
  return (
    <Container fluid>
      <Row>
        <Col className="leftcol" md={2}>
          {" "}
          this is left col
        </Col>
        <Col className="leftcolumn">hello</Col>
      </Row>
    </Container>
  );
}

export default LeftCol;
