import React, {useState,useRef,useEffect} from 'react';
import {MapContainer, TileLayer, Marker, Popup,Polygon,Rectangle,FeatureGroup,Circle} from "react-leaflet";
import {EditControl} from 'react-leaflet-draw';
import L from "leaflet";
import "leaflet/dist/leaflet.css";
import {Container,Col, Row,Button,Form} from 'react-bootstrap'
import * as Icon from "react-bootstrap-icons";
import ListModal from './ListModal'
import './Navbar.css'
import LeftCol from './LeftCol';
import "leaflet/dist/leaflet.css";
import "leaflet-draw/dist/leaflet.draw.css";



const markerIcon = new L.Icon({
  iconUrl: require("./resources/point.jpg"),
  iconSize: [40, 40],
  iconAnchor: [17, 46], //[left/right, top/bottom]
  popupAnchor: [0, -46], //[left/right, top/bottom]
});

function Map() {
    const [fetchlatlong, setfetchlatlong] = useState({
      firstlatlong: "",
      secondlatlong: "",
      thirdlatlong: "",
    });
    const [xCord, setXCord] = useState("");
    const [yCord, setYCord] = useState("");
    const [coordinates, setCoordinates] = useState([]);
    const handleChangeXCord = (e) => setXCord(e.target.value);
    const handleChangeYCord = (e) => setYCord(e.target.value);
    const [dirstate, dirsetstate] = useState([]);
    const [pointadd, setpointadd] = useState([]);
    const [pointname, setpointname] = useState([]);
    const [pstate, setpstate] = useState([]);
    const [mark, setmarkdrag] = useState("false");

  


  const resetStates = () => {
    setXCord("");
    setYCord("");
  };
   
  const mapRef = useRef();
  const _created = (e) => console.log(e);

  const _submit = (e) => {
    e.preventDefault();
    const coordinateObj = {xCord, yCord, id: Math.random()};
    setCoordinates([...coordinates, coordinateObj]);
    
    // console.log("hello", [...coordinates, coordinateObj]);
    coordinates.map((p) => {
      console.log("p", [Number(p.xCord), Number(p.yCord)]);
    });
    // console.log(coordinates);
  };

  const handleDel = (todoKey) => (e) => {
    console.log(todoKey);
    setCoordinates((prev) => prev.filter((todo) => todo.id !== todoKey));
  };

    
    var pointaddhere=[]
    var nopointadd=[]
    function handlechangepoint (){
      setpointadd((prevState) => [...prevState, pointname],
      ()=>console.log('hello',pointadd))
      console.log(data)
      pointadd.map((p)=>(
        setpstate((prevState)=>[...prevState,[p.toString()]])
       
      ))
      pstate.map((p)=>(console.log('hello',p)))

      console.log('chec',pstate)
    }


    const togglemarker = ()=> {
      setmarkdrag(mark=> !mark)

    }

    function handlechange(e) {
      const value = e.target.value;
      setfetchlatlong({
        ...fetchlatlong,
        [e.target.name]: value,
      });
      console.log(fetchlatlong.firstlatlong)
    }
    const purpleOptions={color: "purple"}
    const redOptions = {color: "red"};
    var polydra = [];

    function createpoint(){
      var p1=fetchlatlong.firstlatlong.split(',',2)
       var lat1 = parseFloat(p1[0]);
       var lng1 = parseFloat(p1[1]);
       var p2 = fetchlatlong.secondlatlong.split(",", 2);
       var lat2 = parseFloat(p2[0]);
       var lng2 = parseFloat(p2[1]);
      const p3=fetchlatlong.thirdlatlong.split(',',2)
      const lat3 = parseFloat(p3[0]);
      const lng3 = parseFloat(p3[1]);

      polydra=[[lat1,lng1],[lat2,lng2],[lat3,lng3],]
      console.log("here it is",polydra);
      dirsetstate(polydra)
      console.log('poly',polydra)
      
    }
    var position=[]
   
    var data=[]
     
  return (
    <Container className="mapcontain" fluid>
      <Row>
        <Row>
          <Col className="mapset" md={9}>
            <MapContainer
              center={[51.49, -0.08]}
              zoom={6}
              style={{
                height: "71vh",
                width: "178%",
              }}
              scrollWheelZoom={true}
              minZoom={0}
              maxBoundsViscosity={1.0}
              attributionControl={true}
              animate={true}
            >
              <TileLayer
                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
              />
              <FeatureGroup color="purple">
                {coordinates && coordinates.length > 1
                  ? [...coordinates, {xCord, yCord}].map((p) => (
                      <Marker
                        position={[Number(p.xCord), Number(p.yCord)]}
                        draggable={mark}
                        icon={markerIcon}
                      ></Marker>
                    ))
                  : ""}
                {dirstate && dirstate.length > 1 ? (
                  <Polygon color={"green"} positions={dirstate} />
                ) : (
                  " "
                )}

                <EditControl
                  position="topright"
                  // onEdited={this._onEditPath}
                  onCreated={_created}
                  // onDeleted={this._onDeleted}
                  draw={{
                    rectangle: false,
                  }}
                />
              </FeatureGroup>
            </MapContainer>
          </Col>
        </Row>
        <Col md={2} className="arrangecolumn">
          <Row>
            <Col>
              <form>
                <input
                  className="inpfield"
                  placeholder="Enter Lat,Long"
                  value={fetchlatlong.firstlatlong}
                  onChange={handlechange}
                  name="firstlatlong"
                />
                <input
                  className="inpfield"
                  placeholder="Enter Lat,Long"
                  value={fetchlatlong.secondlatlong}
                  onChange={handlechange}
                  name="secondlatlong"
                />
                <input
                  className="inpfield"
                  placeholder="Enter Lat,Long"
                  value={fetchlatlong.thirdlatlong}
                  onChange={handlechange}
                  name="thirdlatlong"
                />
              </form>
              
              <div>
                {coordinates && coordinates.length ? (
                  <ol>
                    {coordinates.map((todo, Index) => (
                      <li key={todo.id}>
                        {" "}
                        {todo.xCord},{todo.yCord}
                        <Icon.ArrowRight className="iconclick" onClick={handleDel(todo.id)} />
                        {/* <button type="button" onClick={handleDel(todo.id)}>
                          Remove
                        </button> */}
                      </li>
                    ))}{" "}
                  </ol>
                ) : (
                  ""
                )}
              </div>
              <form>
                <input
                  className="inpfield"
                  placeholder="Lat for point"
                  onChange={handleChangeXCord}
                />

                <br />
                <input
                  className="inpfield"
                  placeholder="Lat for point"
                  onChange={handleChangeYCord}
                />
               
                <br />
              </form>
              <div className="btndiv">
                {" "}
                <Button className="btnbe" variant="outline-warning" size="sm" onClick={_submit}>
                  Points
                </Button>
                <Button className="btnbe" variant="outline-warning" size="sm" onClick={createpoint}>
                  Polygon
                </Button>
                <Button
                  className="btnbe"
                  variant="outline-warning"
                  size="sm"
                  onClick={togglemarker}
                >
                  Move
                </Button>
                <Button className="btnbe" variant="outline-warning" size="sm" onClick={_submit}>
                  Make grid
                </Button>
                <ListModal />
              </div>

              {/* <Button variant="outline-dark">Create Point</Button> */}
            </Col>
          </Row>
        </Col>
      </Row>
    </Container>
  );
}

export default Map;


