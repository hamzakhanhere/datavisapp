import React from 'react';
import {Container, Row, Col, Navbar, Nav} from "react-bootstrap";
import './Navbar.css';

function Navb() {
  return (
    <div>
      <Navbar className="navbarstyle" expand="lg">
        <Container>
          <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link href="#home">
                <h1 className="elem ">Timeline</h1>
              </Nav.Link>
              <Nav.Link href="#tables">
                <h1 className="elem">Tables</h1>
              </Nav.Link>
              <Nav.Link href="#charts">
                <h1 className="elem">Charts</h1>
              </Nav.Link>
              <Nav.Link href="#maps">
                <h1 className="elem">Maps</h1>
              </Nav.Link>
              <Nav.Link href="#locations">
                <h1 className="elem">Locations</h1>
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  );
}

export default Navb;
