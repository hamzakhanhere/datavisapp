import React,{useState} from 'react'
import {Button,Modal} from 'react-bootstrap'

function ListModal() {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
   const a =['a','b','<h1>c</h1>','<h2>d</h2>','<>']

  return (
    <>
      <Button variant="primary" onClick={handleShow}>
        Select Point Type
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Greenhouse Crops</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ol>
            <li>Tomato</li>
            <li>Pepper</li>
          </ol>
        </Modal.Body>

        <Modal.Header>
          <Modal.Title>Field Crops</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ol>
            <li>Bean</li>
            <li>Corn</li>
            <li>Potato</li>
          </ol>
        </Modal.Body>
        <Modal.Header >
          <Modal.Title>Orchard Crops</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ol>
            <li>Apple</li>
            <li>Pear</li>
            <li>Orange</li>
          </ol>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleClose}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}


export default ListModal