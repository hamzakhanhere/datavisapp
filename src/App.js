import Navb from "./Components/Navbar";
import LeftCol from "./Components/LeftCol";
import RightCol from "./Components/RightCol";
import Map from "./Components/Map";
import Sidebar from "./Components/Sidebar"
import TopLongLeft from "./Components/TopLongLeft"

function App() {
  // const [name,setName]=LocalStoragehook('name','')
  return (
    <>
      <Navb />
      
      <LeftCol />
      <Map />
    </>
  );
}

export default App;
